package com.bignerdranch.android.criminalintent;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.hardware.Camera.Size;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * Created by Athene on 9/29/2014.
 */
public class CrimeCameraFragment extends Fragment {
    private static final String TAG = "CrimeCameraFragment";

    private Camera mCamera;
    private SurfaceView mSurfaceView;

    private View mProgressContainer;

    private Camera.ShutterCallback mShutterCallback = new Camera.ShutterCallback(){
        public void onShutter(){
            mProgressContainer.setVisibility(View.VISIBLE);
        }
    };

    private Camera.PictureCallback mJpegCallback = new Camera.PictureCallback(){
        public void onPictureTaken(byte[] data, Camera camera){
            //Create a filename
            String filename = UUID.randomUUID().toString()+".jpg";
            //save the jpeg data to disk
            FileOutputStream os = null;
            boolean success = true;

            try{
                os = getActivity().openFileOutput(filename, Context.MODE_PRIVATE);
                os.write(data);

            }catch(Exception e){
                Log.e(TAG, "Error writing to file "+ filename, e);
                success=false;
            }finally{
                try{
                    if(os != null)
                        os.close();
                }catch (Exception e){
                    Log.e(TAG, "Error closing file "+filename, e);
                    success = false;
                }
            }
            if(success){
                Log.i(TAG, "JPEG saved at "+ filename);
            }
            getActivity();
        }
    };

    @Override
    @SuppressWarnings("deprecation")
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState){
        View v=inflater.inflate(R.layout.fragment_crime_camera, parent, false);

        mProgressContainer = v.findViewById(R.id.crime_camera_progressContainer);
        mProgressContainer.setVisibility(View.INVISIBLE);

        Button takePictureButton=(Button)v
                .findViewById(R.id.crime_camera_takePictureButton);
        takePictureButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                if(mCamera != null){
                    mCamera.takePicture(mShutterCallback, null, mJpegCallback);
                }
            }
        });
        mSurfaceView=(SurfaceView)v.findViewById(R.id.crime_camera_surfaceView);
        SurfaceHolder holder = mSurfaceView.getHolder();
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        //this interface listens for events in the lifecucle of a surface so that
        //you can coordinate the surface with its client
        holder.addCallback(new SurfaceHolder.Callback(){
            //onfcre is called when the view hiearchy that the surfaceView
            //belongs is put on the screen
            public void surfaceCreated(SurfaceHolder holder){
                //tell the camrea to use this surface as its preview area
                try{
                    if(mCamera !=null){
                        mCamera.setPreviewDisplay(holder);
                    }
                }catch (IOException exception){
                    Log.e(TAG, "Error setting yp preview display", exception);
                }
            }
            //is called when the surfaceView is removed from the screen,
            //the surface is destryed.
            public void surfaceDestroyed(SurfaceHolder holder){
            //stop the preview
            if(mCamera !=null){
                mCamera.stopPreview();
            }
        }
            //when the surface is being displayed for the first time this will be called
            public void surfaceChanged(SurfaceHolder holder, int format, int w, int h){
                if(mCamera == null)return;
                Camera.Parameters parameters = mCamera.getParameters();
               Size s = getBestSupportedSize(parameters.getSupportedPreviewSizes(),w,h);
                parameters.setPreviewSize(s.width, s.height);
                s = getBestSupportedSize(parameters.getSupportedPictureSizes(),w,h);
                parameters.setPictureSize(s.width,s.height);
                mCamera.setParameters(parameters);
                try{
                    mCamera.startPreview();
                }catch (Exception e)
                {
                    Log.e(TAG, "Could not start preview", e);
                    mCamera.release();
                    mCamera = null;
                }
            }
        });

        return v;
    }

    @TargetApi(9)
    @Override
    public void onResume(){
        super.onResume();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD){
            mCamera=Camera.open(0);
        }else{
            mCamera=Camera.open();
        }
    }
    public void onPause(){
        super.onPause();
        if(mCamera!=null){
            mCamera.release();
            mCamera=null;

        }
    }
    private Size getBestSupportedSize(List<Size> sizes, int width, int height){
        Size bestSize = sizes.get(0);
        int largestArea = bestSize.width * bestSize.height;
        for(Size s : sizes){
            int area = s.width * s.height;
            if(area > largestArea){
                bestSize = s;
                largestArea  = area;
            }
        }
        return bestSize;
    }
}
